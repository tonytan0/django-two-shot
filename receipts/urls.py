from django.urls import path
from receipts.views import (
    ReceiptListView,
    AccountCreateView,
    AccountListView,
    ExpenseCategoryCreateView,
    ExpenseCategoryListView,
    ReceiptCreateView,
)


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="create_accounts"
    ),
    path("accounts/", AccountListView.as_view(), name="account_list"),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="expenses_create",
    ),
    path(
        "categories/", ExpenseCategoryListView.as_view(), name="expenses_list"
    ),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
]
